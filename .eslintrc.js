const os = require('os');

const options = {
  extends: [
    'airbnb',
    'prettier',
    'prettier/react',
    'plugin:jest/recommended',
    'plugin:react/recommended',
    'plugin:prettier/recommended',
    'plugin:@typescript-eslint/recommended'
  ],
  plugins: ['prettier', 'jest', 'react', '@typescript-eslint'],
  env: {
    node: true,
    'jest/globals': true,
    es6: true
  },
  parserOptions: {
    ecmaVersion: 2018
  },
  rules: {
    'camelcase': 'off',
    '@typescript-eslint/no-unused-vars': 'error',
    'no-alert': 'error',
    'no-eq-null': 'error',
    'no-useless-call': 'error',
    'prefer-promise-reject-errors': 'error',
    'react/prop-types': 'error',
    'react/state-in-constructor': 'off',
    'react/jsx-props-no-spreading': 'off',
    'react/require-default-props': 'error',
    'react/button-has-type': 'error',
    'no-underscore-dangle': 'error',
    'react/static-property-placement': ['warn', 'static public field'],
    'linebreak-style': os.EOL === '\n' ? ['error', 'unix'] : 'off',
    'react/jsx-no-bind': [
      'error',
      {
        ignoreDOMComponents: false,
        allowArrowFunctions: true,
        allowFunctions: false,
        allowBind: false
      }
    ],
    'prefer-const': [
      'error',
      {
        destructuring: 'any',
        ignoreReadBeforeAssign: false
      }
    ],
    'no-use-before-define': [
      'error',
      { functions: true, classes: true, variables: true }
    ],
    'import/no-extraneous-dependencies': ['error', { devDependencies: true }],
    'import/imports-first': ['error', 'absolute-first'],
    'react/jsx-filename-extension': [1, { extensions: ['.ts', '.tsx', '.js', '.jsx'] }],
    'import/extensions': [0],
    'react/jsx-fragments': ['error', 'element']
  },
  settings: {
    'import/extensions': ['.js','.jsx','.ts','.tsx'],
    'import/parsers': {
      '@typescript-eslint/parser': ['.ts','.tsx']
    },
    'import/resolver': {
      'node': {
        'extensions': ['.js','.jsx','.ts','.tsx']
      }
    }
  },
  globals: {
    window: true,
    document: true,
    localStorage: true,
    FormData: true,
    FileReader: true,
    Blob: true,
    navigator: true
  },
  parser: '@typescript-eslint/parser'
};

module.exports = options;
