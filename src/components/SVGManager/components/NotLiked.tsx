import React from 'react';
import Svg, {Defs, Path, G, Use} from 'react-native-svg';
import SVGProps from './interface';

const NotLiked = ({width, height}: SVGProps) => (
  <Svg width={width || 20} height={height || 20} viewBox="0 0 26 24">
    <Defs>
      <Path
        d="M13.82 2.5c2.774 0 4.513 2.392 4.513 5.455 0 3.897-3.692 7.584-8.08 9.438L10 17.5l-.254-.107c-4.387-1.854-8.08-5.541-8.08-9.438 0-3.063 1.74-5.455 4.515-5.455 1.601 0 2.618.526 3.819 1.723C11.201 3.026 12.218 2.5 13.82 2.5zm3.124 5.455c0-2.307-1.202-3.96-3.125-3.96-1.358 0-2.128.489-3.325 1.794L10 6.33l-.494-.54C8.309 4.484 7.539 3.994 6.18 3.994c-1.923 0-3.125 1.654-3.125 3.96 0 3.046 3.143 6.252 6.944 7.938 3.8-1.686 6.944-4.892 6.944-7.937z"
        id="prefix__b"
      />
    </Defs>
    <G transform="translate(1)" fill="none" fillRule="evenodd">
      <Path d="M0 0h20v20H0z" />
      <Use fill="#FFF" xlinkHref="#prefix__b" />
    </G>
  </Svg>
);

export default NotLiked;
