import React from 'react';
import Svg, {Path} from 'react-native-svg';
import SVGProps from './interface';

const BackArrow = ({width, fill, height}: SVGProps) => (
  <Svg width={width || 20} height={height || 16} viewBox="0 0 20 16">
    <Path
      d="M8.58.24a.81.81 0 0 1 0 1.136L2.764 7.19l16.433.005c.447.001.803.357.803.804a.808.808 0 0 1-.803.814L2.764 8.81l5.816 5.804c.31.32.31.837 0 1.146a.804.804 0 0 1-1.147 0L.24 8.568a.79.79 0 0 1 0-1.135L7.433.24a.804.804 0 0 1 1.147 0z"
      fill={fill || '#333'}
      fillRule="evenodd"
    />
  </Svg>
);

export default BackArrow;
