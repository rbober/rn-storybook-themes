export default interface SVGProps {
  width: number;
  height: number;
  fill?: string;
}
