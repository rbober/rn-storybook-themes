import React from 'react';
import Svg, {Defs, Path, G, Use} from 'react-native-svg';
import SVGProps from './interface';

const Liked = ({width, height}: SVGProps) => (
  <Svg width={width || 20} height={height || 20} viewBox="0 0 26 24">
    <Defs>
      <Path
        d="M14.305 2.529c-1.812-.21-2.958.748-4.305 2.245C8.611 3.277 7.507 2.35 5.695 2.53c-2.458.322-4.388 3.346-3.972 5.988C2.418 12.754 5.89 15.254 10 17.5c4.166-2.246 7.638-4.746 8.277-8.983.416-2.642-1.514-5.666-3.972-5.988z"
        id="prefix__b"
      />
    </Defs>
    <G
      transform="translate(1)"
      fill="none"
      fillRule="evenodd">
      <Path d="M0 0h20v20H0z" />
      <Use fill="#FF5722" xlinkHref="#prefix__b" />
    </G>
  </Svg>
);

export default Liked;
