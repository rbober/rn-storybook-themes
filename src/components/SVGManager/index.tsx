import React from 'react';
import SVGProps from './components/interface';

import BackArrow from './components/BackArrow';
import NotLiked from './components/NotLiked';
import Liked from './components/Liked';

const BackArrowIcon = (props: SVGProps) => <BackArrow {...props} />;
const NotLikedIcon = (props: SVGProps) => <NotLiked {...props} />;
const LikedIcon = (props: SVGProps) => <Liked {...props} />;

export default class SVGManager {
  static MANIFEST: {
    BACKICON: string;
    NOTLIKED: string;
    LIKED: string;
  } = {
    BACKICON: 'BACKICON',
    NOTLIKED: 'NOTLIKED',
    LIKED: 'LIKED',
  };

  static ICONS: {
    [type: string]: React.FC<SVGProps>;
  } = {
    BACKICON: BackArrowIcon,
    NOTLIKED: NotLikedIcon,
    LIKED: LikedIcon,
  };

  static getIconFactory(type: string): React.FC<SVGProps> {
    if (!SVGManager.ICONS[type]) {
      console.log('Wrong ', type);
    }
    return SVGManager.ICONS[type] || null;
  }
}
