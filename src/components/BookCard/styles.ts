import styled from 'styled-components/native';

export const CardWrapper = styled.TouchableOpacity`
  margin-right: 10px;
  max-width: 150px;
  position: relative;
`;

export const CardImage = styled.Image`
  width: 158px;
  height: 106px;
  border-radius: 2px;
`;

export const CategoryTex = styled.Text`
  color: ${({theme}) => theme.color.mainColor};
  font-size: 11px;
  font-weight: 600;
`;

export const DefaultImage = styled.View`
  width: 158px;
  height: 106px;
  background-color: ${({theme}) => theme.color.mainColor};
  border-radius: 2px;
`;

export const CardTitle = styled.Text`
  font-size: 14px;
  font-weight: 500;
  color: ${({theme}) => theme.color.textBlack};
`;

export const CardHeart = styled.View`
  position: absolute;
  top: 6px;
  right: 0;
  z-index: 10;
`

