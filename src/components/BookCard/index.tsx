import React from 'react';
import SVGManager from '../SVGManager';
import {
  CardWrapper,
  CardImage,
  CategoryTex,
  DefaultImage,
  CardTitle,
  CardHeart,
} from './styles';

interface Props {
  category: string;
  title: string;
  image?: string | null;
  liked?: boolean;
  index: number | null;
}

const BookCard = ({category, title, image, liked, index}: Props) => {
  return (
    <CardWrapper>
      {image ? (
        <CardImage source={{uri: `https:${image}`}} />
      ) : (
        <DefaultImage />
      )}
      <CategoryTex>{category.toUpperCase()}</CategoryTex>
      <CardTitle>{title}</CardTitle>
      <CardHeart>
        {SVGManager.getIconFactory(
          liked ? SVGManager.MANIFEST.LIKED : SVGManager.MANIFEST.NOTLIKED,
        )({width: 20, height: 20})}
      </CardHeart>
    </CardWrapper>
  );
};

export default BookCard;
