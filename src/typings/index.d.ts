import {DefaultTheme} from 'styled-components/native';

declare module 'styled-components' {
  export interface DefaultTheme {
    color: {
      mainBackgroundColor: string;
      mainOrange: string;
      defaultTextColor: string;
      secondaryTextColor: string;
      black: string;
      defaultOrange: string;
      defaultGrey: string;
      defaultWhite: string;
      defaultBackgroundColor: string;
      lightGrey: string;
      sectionGrey: string;
      defaultBlack: string;
      borderColor: string;
      colorGrey: string;
      mainColor: string;
      textBlack: string;
      themeBg: string;
    };
    gradient: [string, string];
  }
}
