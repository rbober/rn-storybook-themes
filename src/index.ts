import BookCard from './components/BookCard';
import SVGManager from './components/SVGManager';

export default {
  BookCard,
  SVGManager,
};
