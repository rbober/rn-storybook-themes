import {DefaultTheme} from 'styled-components';

const LightTheme: DefaultTheme = {
  color: {
    mainBackgroundColor: '#ff5500',
    mainOrange: '#ff5500',
    defaultTextColor: '#434c57',
    secondaryTextColor: '#b1b1b1',
    black: '#333333',
    defaultOrange: '#FF5E3A',
    defaultGrey: '#999999',
    defaultWhite: '#FFF',
    defaultBackgroundColor: '#FFF',
    lightGrey: '#E5E5E5',
    sectionGrey: '#F4F4F4',
    defaultBlack: '#333',
    borderColor: 'rgb(218, 218, 218)',
    colorGrey: '#6e767f',
    mainColor: '#008081',
    textBlack: '#3C3C3C',
    themeBg: '#fff',
  },
  gradient: ['#FF6243', '#FF0072'],
};

const DarkTheme: DefaultTheme = {
  color: {
    mainBackgroundColor: '#000',
    mainOrange: '#ff5500',
    defaultTextColor: '#fff',
    secondaryTextColor: '#b1b1b1',
    black: '#333333',
    defaultOrange: '#FF5E3A',
    defaultGrey: '#999999',
    defaultWhite: '#FFF',
    defaultBackgroundColor: '#FFF',
    lightGrey: '#E5E5E5',
    sectionGrey: '#F4F4F4',
    defaultBlack: '#333',
    borderColor: 'rgb(218, 218, 218)',
    colorGrey: '#6e767f',
    mainColor: '#fff',
    textBlack: '#3C3C3C',
    themeBg: '#000',
  },
  gradient: ['#FF6243', '#FF0072'],
};

export default {LightTheme, DarkTheme};
