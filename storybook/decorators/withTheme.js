import React from 'react';
import {View, TouchableOpacity, SafeAreaView, Text} from 'react-native';
import {ThemeProvider} from 'styled-components/native';
import theme from '../../src/themes';

const TextStyle = {
  fontSize: 20,
};

const TextWrapper = {
  width: 80,
  height: 35,
  justifyContent: 'center',
  alignItems: 'center',
  paddingHorizontal: 5,
  marginRight: 10,
  borderWidth: 1,
  borderColor: '#c2c2c2',
  backgroundColor: '#fff',
};

class ThemeDecorator extends React.Component {
  state = {
    isLight: false,
  };

  setLight = value => {
    this.setState({isLight: value});
  };

  render() {
    const {isLight} = this.state;
    // eslint-disable-next-line react/prop-types
    const {storyFn} = this.props;
    return (
      <ThemeProvider theme={isLight ? theme.LightTheme : theme.DarkTheme}>
        <SafeAreaView
          style={{
            flex: 1,
          }}>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: isLight
                ? theme.LightTheme.color.themeBg
                : theme.DarkTheme.color.themeBg,
            }}>
            {storyFn()}
          </View>
          <View
            style={{
              height: 100,
              width: '100%',
              position: 'absolute',
              bottom: 0,
              left: 0,
              flexDirection: 'row',
              justifyContent: 'center',
            }}>
            <TouchableOpacity
              style={TextWrapper}
              onPress={() => this.setLight(true)}>
              <Text style={TextStyle}>Light</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={TextWrapper}
              onPress={() => this.setLight(false)}>
              <Text style={TextStyle}>Dark</Text>
            </TouchableOpacity>
          </View>
        </SafeAreaView>
      </ThemeProvider>
    );
  }
}

export default function WithTheme(storyFn) {
  return <ThemeDecorator storyFn={storyFn} />;
}
