// Auto-generated file created by react-native-storybook-loader
// Do not edit.
//
// https://github.com/elderfo/react-native-storybook-loader.git

function loadStories() {
	require('./stories/input.story');
}

const stories = [
	'./stories/input.story'
];

module.exports = {
  loadStories,
  stories,
};
