import React from 'react';
import {View} from 'react-native';
import {storiesOf} from '@storybook/react-native';
import BookCard from '../../src/components/BookCard';
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
import themeDecorator from '../decorators/withTheme';

storiesOf('Book Card', module)
  .addDecorator(themeDecorator)
  .add('default card with like', () => (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <BookCard
        category="krim og thrillere"
        title="Høstens heiteste boktips"
        liked
        index={0}
      />
    </View>
  ))
  .add('card without like', () => (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <BookCard
        category="krim og thrillere"
        title="Høstens heiteste boktips"
        liked={false}
        index={0}
      />
    </View>
  ));
